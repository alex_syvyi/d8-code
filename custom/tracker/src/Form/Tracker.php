<?php

namespace Drupal\tracker\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form to configure tracker.
 */
class Tracker extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'tracker_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'tracker.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('tracker.settings');
    $status = [
      $this->t('Disabled'),
      $this->t('Disabled'),
    ];
    $paths = [
      $this->t('All pages except those listed'),
      $this->t('Only the listed pages'),
    ];
    $options = [
      $this->t('All roles except those listed'),
      $this->t('Only the listed roles'),
    ];
    $roles = user_role_names();
    $form['status'] = [
      '#type' => 'radios',
      '#title' => $this->t('Tracker status'),
      '#options' => $status,
      '#default_value' => $config->get('status'),
    ];
    $form['analytics_path_toggle'] = [
      '#type' => 'radios',
      '#title' => $this->t('Include tracker on specific pages'),
      '#options' => $paths,
      '#default_value' => $config->get('analytics_path_toggle'),
    ];
    $description = $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", [
      '%blog' => 'blog',
      '%blog-wildcard' => 'blog/*',
      '%front' => '<front>',
    ]);
    $form['analytics_path_list'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Pages'),
      '#description' => $description,
      '#default_value' => $config->get('analytics_path_list'),
    ];
    $form['analytics_role_toggle'] = [
      '#type' => 'radios',
      '#title' => $this->t('Add tracker for specific roles'),
      '#options' => $options,
      '#default_value' => $config->get('analytics_role_toggle'),
    ];
    $form['analytics_role_list'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Selected roles'),
      '#options' => $roles,
      '#default_value' => $config->get('analytics_role_list'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $config = $this->config('tracker.settings');
    $config->set('status', $values['status']);
    $config->set('analytics_path_toggle', $values['analytics_path_toggle']);
    $config->set('analytics_path_list', $values['analytics_path_list']);
    $config->set('analytics_role_toggle', $values['analytics_role_toggle']);
    $config->set('analytics_role_list', $values['analytics_role_list']);
    $config->save();
  }

}

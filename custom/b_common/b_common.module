<?php

/**
 * @file
 * File for custom hooks and code.
 */

use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_language_switch_links_alter().
 */
function b_common_language_switch_links_alter(array &$links, $type, Url $url) {
  unset($links['en-ZA'], $links['en-AU'], $links['en-SG']);
}

/**
 * Implements hook_form_alter().
 */
function b_common_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  if (isset($form['#webform_id']) && $form['#webform_id'] === 'contact') {
    $data = getData();
    $grouped = groupByCategory($data);

    // Group data by category.
    foreach ($grouped as $categoryName => $category) {
      $sorted = getNested($category, $categoryName);
      $unique = getUniques($sorted);
      $filtered[$categoryName] = reset($unique);
    }

    // Prepare data $form to render.
    foreach ($filtered as $group => $elements) {
      foreach ($elements as $element) {
        $form['elements']['product']['#options'][$group][$element['nid']] = $element['title'];
      }
    }
    array_shift($form['elements']['product']['#options']);
  }
}

function getData() {
  $data = [];
  $database = \Drupal::database();
  $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
  $query = $database->select('node__field_category', 'nfc');
  $query->join('node_field_data', 'nfd', 'nfd.nid = nfc.entity_id');
  $query->condition('nfd.langcode', $language, 'IN')
    ->fields('nfd', ['nid', 'title'])
    ->addField('nfc', 'field_category_target_id', 'category');
  $result = $query->execute();

  while ($row = $result->fetch()) {
    $data[$row->category][] = [
      'title' => $row->title,
      'nid' => $row->nid,
    ];
  }

  return $data;
}

function groupByCategory($data) {
  $groupedByCategory = [];
  foreach ($data as $cid => $value) {
    $terms = \Drupal::service('entity_type.manager')->getStorage("taxonomy_term")->loadAllParents($cid);
    $parent = end($terms);
    if ($parent) {
      $groupedByCategory[$parent->getName()][] = $value;
    }
  }
  return $groupedByCategory;
}

function getNested($array, $category) {
  $allNested = [];
  foreach ($array as $key => $elements) {
    if (count($elements) > 1) {
      foreach ($elements as $element) {
        $allNested[$category][] = $element;
      }
    }
    else {
      $allNested[$category][] = reset($elements);
    }
  }
  return $allNested;
}

function getUniques($array) {
  $filtered = [];
  foreach ($array as $key => $value) {
    $filtered[$key] = array_unique($value, SORT_REGULAR);
  }
  return $filtered;
}

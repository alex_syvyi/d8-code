<?php

/**
 * @file
 * Defines features and functions common to the ff_xmlsitemap_free.
 */

/**
 * Implements hook_menu().
 *
 * @uses ff_xmlsitemap_free_free_articles
 * @uses ff_xmlsitemap_free_single_sitemap
 */
function ff_xmlsitemap_free_menu() {
  $items = [];

  $items['articles-sitemap.xml'] = [
    'title' => t('XML Free articles'),
    'description' => t('Generate site map based on free articles.'),
    'page callback' => 'ff_xmlsitemap_free_free_articles',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
    'weight' => 21,
  ];

  $items['articles-sitemap/%/article-sitemap.xml'] = [
    'page callback' => 'ff_xmlsitemap_free_single_sitemap',
    'page arguments' => array(1),
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  ];

  return $items;
}

/**
 * Implements hook_node_presave().
 */
function ff_xmlsitemap_free_node_presave($node) {
  if ($node->type === 'article') {
    $original_rss = $node->original->field_article_rss_feed[LANGUAGE_NONE][0]['value'] ?? '';
    $current_rss = $node->field_article_rss_feed[LANGUAGE_NONE][0]['value'] ?? '';
    $original_body = $node->original->body[LANGUAGE_NONE][0]['value'] ?? '';
    $current_body = $node->body[LANGUAGE_NONE][0]['value'] ?? '';
    // Check if bundle 'article, node changes were in published status, or status changed when rss == 0.
    if (($node->status == 1 && ($current_rss != $original_rss || ($node->is_new && $current_rss == '0') || $original_body !== $current_body))
      ||
      ($current_rss == '0' && $node->status != $node->original->status))
     {
      variable_set('free_articles_sitemap_rebuild', TRUE);
    }
  }
}

/**
 * Menu callback.
 */
function ff_xmlsitemap_free_free_articles() {
  global $base_url;

  module_load_include('inc', 'xmlsitemap', 'xmlsitemap.pages');

  $rebuild = variable_get('free_articles_sitemap_rebuild', TRUE);

  $query = db_select('node', 'n');
  $query->leftJoin('field_data_body', 'fdb', 'n.nid = fdb.entity_id AND fdb.entity_type = :node AND fdb.deleted = :del', [':node' => 'node', ':del' => '0']);
  $query->leftJoin('field_data_field_article_rss_feed', 'f_rss', "n.nid = f_rss.entity_id AND f_rss.field_article_rss_feed_value = '1'");
  $query->fields('n', ['title', 'created', 'nid'])
    ->condition('n.status', '1')
    ->condition('n.type', 'article')
    ->condition('fdb.body_value', '\\[subscribe\\]|\\[subscribe_basic\\]|\\[subscribe_dfs\\]', 'NOT RLIKE')
    ->isNull('f_rss.field_article_rss_feed_value')
    ->orderBy('n.created', 'DESC');
  $result = $query->execute()->fetchAll();
  $result_count = count($result);
  $pages = intdiv($result_count, 1000) + 1;

  // Check if file exist to avoid risk of Page Not Found when user manually remove file.
  $file = file_build_uri('xmlsitemap/articles-sitemap/articles-sitemap.xml');
  if ($result && $rebuild || !file_exists($file)) {

    // Create main site map file.
    $xml_general = ff_file_directory_prepare('/articles-sitemap/', 'articles-sitemap.xml', 'sitemapindex');
    // If pages > then 1, create few separate site map files.
    if ($pages > 1) {
      // Create separate file for every 1000 items.
      for ($i = 1; $pages >= $i; $i++) {
        $file_path = $base_url . '/articles-sitemap/' . $i . '/article-sitemap.xml';
        ff_add_sitemap_item($xml_general, 'sitemap', $file_path);
        $sliced = array_slice($result, ($i - 1) * 1000, 1000);
        $xml = ff_file_directory_prepare('/articles-sitemap/' . $i, 'article-sitemap.xml', 'urlset');
        ff_generate_sitemap_content($sliced, $xml);
      }
    }
    else {
      ff_generate_sitemap_content($result, $xml_general);
    }
    $xml_general->endDocument();
  }

  variable_set('free_articles_sitemap_rebuild', FALSE);

  return xmlsitemap_output_file($file);
}

/**
 * Menu callback.
 *
 * @param $arg
 *   Sitemap directory number.
 *
 * @return mixed
 *   Return generated site map file.
 */
function ff_xmlsitemap_free_single_sitemap($arg) {
  global $base_url;

  module_load_include('inc', 'xmlsitemap', 'xmlsitemap.pages');

  $file = file_build_uri('xmlsitemap/articles-sitemap/' . $arg . '/article-sitemap.xml');

  if (!file_exists($file)) {
    $sitemap_path = $base_url . '/articles-sitemap.xml';
    $message = 'This site map file was not found. Try to regenerate site map files <b><a href="' . $sitemap_path . '">Here</a></b>';
    return [
      '#markup' => $message,
    ];
  }

  return  xmlsitemap_output_file($file);
}

/**
 * Prepares directory and sitemap file.
 *
 * @param string $directory_path
 *   Directory for site map files.
 * @param string $file
 *   File for site map content.
 * @param string $type
 *   Type of site map.
 *
 * @return XMLWriter $xml
 *   Return XMLWriter object.
 */
function ff_file_directory_prepare(string $directory_path, string $file, string $type): XMLWriter {
  $directory_path = file_build_uri('xmlsitemap' . $directory_path);
  file_prepare_directory($directory_path, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS);

  $xml = new XMLWriter();
  $xml->openUri($directory_path . '/' . $file);
  ff_xmlsitemap_start_document($xml, '1.0', 'UTF-8', $type);

  return $xml;
}

/**
 * Prepares and add item to sitemap file.
 *
 * @param XMLWriter $xml
 *   XMLWriter object.
 * @param string $item_type
 *   Type of the item.
 * @param string $file_path
 *   File path for site map content.
 */
function ff_add_sitemap_item(XMLWriter $xml, string $item_type, string $file_path) {
  $xml->startElement($item_type);
  $xml_content = format_xml_elements(['loc' => $file_path]);
  // Remove additional spaces from the output.
  $xml_content = str_replace([" <", ">\n"], ["<", ">"], $xml_content);
  $xml->writeRaw($xml_content);
  $xml->endElement();
  $xml->writeRaw(PHP_EOL);
}

/**
 * Generates site map content.
 *
 * @param array $items
 *   Array of items.
 * @param XMLWriter $xml
 *   XMLWriter object.
 *
 * @return void
 */
function ff_generate_sitemap_content(array $items, XMLWriter $xml) {
  global $base_url;

  foreach ($items as $key => $node) {
    $alias = rtrim($base_url, '/') . '/' . drupal_get_path_alias('node/' . $node->nid);
    ff_add_sitemap_item($xml, 'url', $alias);
  }

  $xml->endDocument();
}

/**
 * Starts site map file.
 *
 * @param XMLWriter $xml_writer
 *   XMLWriter object.
 * @param string $version
 *   Version of XML.
 * @param string $encoding
 *   Coding.
 * @param string $type
 *    Type of site map.
 *
 * @return bool $result
 *  TRUE on success or FALSE on failure.
 */
function ff_xmlsitemap_start_document(XMLWriter $xml_writer, string $version, string $encoding, string $type): bool {
  $result = $xml_writer->startDocument($version, $encoding);

  if (variable_get('xmlsitemap_xsl', 1)) {
    writeXSL($xml_writer);
  }

  ff_xmlsitemap_start_element($xml_writer, $type, TRUE);

  return $result;
}

/**
 * Starts site map element.
 *
 * @param XMLWriter $xml_writer
 *   XMLWriter object.
 * @param string $type
 *   Type of site map.
 * @param bool $root
 *   TRUE or FALSE.
 */
function ff_xmlsitemap_start_element($xml_writer, $type, $root = FALSE) {
  $xml_writer->startElement($type);

  if ($root) {
    foreach (ff_xmlsitemap_get_attr() as $type => $value) {
      $xml_writer->writeAttribute($type, $value);
    }
    $xml_writer->writeRaw(PHP_EOL);
  }
}

/**
 * Return an array of attributes for the root element of the XML.
 */
function ff_xmlsitemap_get_attr(): array {
  $attributes['xmlns'] = 'http://www.sitemaps.org/schemas/sitemap/0.9';

  if (variable_get('xmlsitemap_developer_mode', 0)) {
    $attributes['xmlns:xsi'] = 'http://www.w3.org/2001/XMLSchema-instance';
    $attributes['xsi:schemaLocation'] = 'http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd';
  }

  return $attributes;
}

/**
 * Add the XML stylesheet to the XML page.
 *
 * @param XMLWriter $xml_writer
 *   XMLWriter object.
 */
function writeXSL($xml_writer) {
  $xml_writer->writePi('xml-stylesheet', 'type="text/xsl" href="' . ff_xmlsitemap_get_sitemap_url('sitemap.xsl', ['protocol_relative' => TRUE]) . '"');
  $xml_writer->writeRaw(PHP_EOL);
}

/**
 * Provides sitemap URL.
 *
 * @param string $path
 *   XSL file.
 * @param array $options
 *   Array of options.
 *
 * @return string
 *   Path to XSL file.
 */
function ff_xmlsitemap_get_sitemap_url(string $path, array $options = []): string {
  $options += [
    'absolute' => TRUE,
    'base_url' => variable_get('xmlsitemap_base_url', $GLOBALS['base_url']),
    'language' => language_default(),
    'alias' => TRUE,
  ];

  if (!empty($options['protocol_relative'])) {
    $options['base_url'] = preg_replace('~^https?:~', '', $options['base_url']);
  }

  return url($path, $options);
}

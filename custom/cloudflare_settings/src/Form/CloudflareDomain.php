<?php

namespace Drupal\cloudflare_settings\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures domain for cloudflare module.
 */
class CloudflareDomain extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'cloudflare_domain';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'cloudflare_settings.cloudflare_domain.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('cloudflare_settings.cloudflare_domain.settings');
    $form['domain'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Domain'),
      '#default_value' => $config->get('domain'),
      '#description' => $this->t("Paste domain for clearing CloudFlare caches. Example: 'https://visualobjects.com/'"),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (substr($form_state->getValue('domain'), -1) !== '/') {
      $form_state->setValue('domain', $form_state->getValue('domain') . '/');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $this->config('cloudflare_settings.cloudflare_domain.settings')
      ->set('domain', $values['domain'])
      ->save();
  }

}

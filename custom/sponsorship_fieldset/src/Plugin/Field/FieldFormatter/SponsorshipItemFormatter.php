<?php

namespace Drupal\sponsorship_fieldset\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the sponsorship formatter.
 *
 * @FieldFormatter(
 *   id = "sponsorship_formatter",
 *   label = @Translation("Referenced term id"),
 *   field_types = {
 *     "sponsorship_fieldset"
 *   }
 * )
 */
class SponsorshipItemFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    foreach ($items as $delta => $item) {
      $element[$delta] = [
        '#type' => 'markup',
        '#markup' => $item->term_id,
      ];
    }
    $element['#cache']['max-age'] = 0;
    return $element;
  }

}

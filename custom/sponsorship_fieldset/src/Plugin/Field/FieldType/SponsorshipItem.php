<?php

namespace Drupal\sponsorship_fieldset\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Provides a field type of sponsorship_fieldset.
 *
 * @FieldType(
 *   id = "sponsorship_fieldset",
 *   label = @Translation("Sponsorship Fieldset"),
 *   module = "sponsorship_fieldset",
 *   description = @Translation("Creates Sponsorship Fieldset"),
 *   category = @Translation("Sponsorship"),
 *   default_widget = "sponsorship_widget",
 *   default_formatter = "sponsorship_formatter"
 * )
 */
class SponsorshipItem extends FieldItemBase implements FieldItemInterface {

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'term_id' => [
          'type' => 'int',
          'not null' => FALSE,
        ],
        'level' => [
          'type' => 'int',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties = [];
    $properties['term_id'] = DataDefinition::create('integer');
    $properties['level'] = DataDefinition::create('string');

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = empty($this->get('term_id')->getValue()) && empty($this->get('level')->getValue());
    return $value;
  }

}

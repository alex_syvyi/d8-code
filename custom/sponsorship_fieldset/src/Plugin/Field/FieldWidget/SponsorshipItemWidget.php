<?php

namespace Drupal\sponsorship_fieldset\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Field\WidgetInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Contains widget for sponsorship fieldset.
 *
 * @FieldWidget(
 *   id = "sponsorship_widget",
 *   label = @Translation("Sponsorship default"),
 *   field_types = {
 *     "sponsorship_fieldset",
 *   }
 * )
 */
class SponsorshipItemWidget extends WidgetBase implements WidgetInterface, ContainerFactoryPluginInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * Constructs a WidgetBase object.
   *
   * @param string $plugin_id
   *   The plugin_id for the widget.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
   *   The definition of the field to which the widget is associated.
   * @param array $settings
   *   The widget settings.
   * @param array $third_party_settings
   *   Any third party settings.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   Entity type manager.
   */
  public function __construct(
    $plugin_id,
    $plugin_definition,
    FieldDefinitionInterface $field_definition,
    array $settings,
    array $third_party_settings,
    EntityTypeManager $entityTypeManager
  ) {
    parent::__construct(
      $plugin_id,
      $plugin_definition,
      $field_definition,
      $settings,
      $third_party_settings
    );
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['third_party_settings'],
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = [];
    $element += [
      '#type' => 'fieldset',
    ];
    $tid = $items[$delta]->term_id;
    $term = $tid ? $this->entityTypeManager->getStorage('taxonomy_term')->load($tid) : NULL;
    $element['term_id'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Sponsorship page'),
      '#target_type' => 'taxonomy_term',
      '#selection_handler' => 'sponsor:taxonomy_term',
      '#selection_settings' => [
        'target_bundles' => ['primary_categories'],
      ],
      '#default_value' => $term,
      '#size' => 70,
    ];

    $element['level'] = [
      '#title' => t('Sponsorship level'),
      '#type' => 'select',
      '#options' => [
        '0' => 'None',
        '1' => 'Starter',
        '2' => 'Bronze',
        '3' => 'Silver',
        '4' => 'Gold',
        '5' => 'Platinum',
        '6' => 'Diamond',
        '7' => 'Double Diamond',
        '8' => 'Triple Diamond',
      ],
      '#default_value' => isset($items[$delta]->level) ? $items[$delta]->level : NULL,
      '#description' => t('Select a sponsorship level'),
    ];

    return $element;
  }

}
